import unittest

import bacteria

# Rules

# 1.  Any live bacteria cell with fewer than two live neighbours dies, as if caused by under-population

# 2.  Any live bacteria cell with two or three live neighbours lives on to the next generation.

# 3.  Any live bacteria cell with more than three live neighbours dies, as if by overcrowding.

# 4.  Any dead bacteria cell with exactly three live neighbours becomes a live bacteria cell, as if by reproduction.

class TestBacteria ( unittest.TestCase ):

    def test_adjacent_cells ( self ):
        self.assertEquals \
            ( neighbouringCell( cell, cells )
            , true
            )

if __name__ == '__main__':
    unittest.main()
