const { stdin, stdout, readAll } = Deno

export const neighbouringCell = ([ ax, ay ], [ bx, by ]) =>
  ax === bx && ay + 1 === by
  || ax === bx && ay - 1 === by
  || ax + 1 === bx && ay === by
  || ax + 1 === bx && ay + 1 === by
  || ax + 1 === bx && ay - 1 === by
  || ax - 1 === bx && ay === by
  || ax - 1 === bx && ay + 1 === by
  || ax - 1 === bx && ay - 1 === by
  || false

export const neighbouringCells = ( cell, cells ) =>
  cells.reduce
    ( ( total, c ) =>
        neighbouringCell( cell, c )
          ? total + 1
          : total
    , 0
    )

export const survivingCells = cells =>
  cells.reduce
    ( ( living, cell ) =>
        {
          const neighbours = neighbouringCells( cell, cells )

          if ( neighbours < 2 || neighbours > 3 )
            return living
          else
            return [
              ...living,
              cell
            ]
        }
    , []
    )

export const cellNeighbours = ([ x, y ]) =>
  [ [ x, y + 1 ]
  , [ x, y - 1 ]
  , [ x + 1, y ]
  , [ x + 1, y + 1 ]
  , [ x + 1, y - 1 ]
  , [ x - 1, y ]
  , [ x - 1, y + 1 ]
  , [ x - 1, y - 1 ]
  ]

export const possibleNeighbours = cells =>
  Array.from
    ( new Set
        ( cells
            .flatMap( cellNeighbours )
            .map( c => JSON.stringify( c ) )
        )
    )
  .map( cellString => JSON.parse( cellString ) )
  // Removes the any new cells that match the existing input cells
  .filter
    ( ([ ax, ay ]) =>
        !cells.find
          ( ([ bx, by ]) =>
              ax === bx && ay === by
          )
    )

export const regeneratedCells = cells =>
  possibleNeighbours( cells )
    .reduce
      ( ( regenerated, cell ) =>
        neighbouringCells( cell, cells ) === 3
          ? [ ...regenerated, cell ]
          : regenerated
      , []
      )

export const parseData = input => input
    .split('\n')
    .filter( line => !( line === 'end' ) )
    .filter(Boolean)
    .map( lines =>
      lines
        .split(',')
        .map( num => parseInt(num) ) )

// STDIO
;( async () => {
  const byteArray = await readAll( stdin );

  const stdinStr = new TextDecoder()

  const stdoutStr = new TextEncoder()

  const inputData = parseData( stdinStr.decode( byteArray ) )

  const nextGeneration =
    [ ...survivingCells( inputData )
    , ...regeneratedCells( inputData )
    ]

  stdout.write
    ( stdoutStr.encode
        ( nextGeneration
            .sort
              ( ([ ax, ay ], [ bx, by ]) =>
                  ax - bx || ay - by
              )
            .join('\n')
            + '\nend'
        )
    )
})()
