import { runTests, test } from "https://deno.land/std/testing/mod.ts"

import {
  assert,
  assertEquals,
  assertThrows
} from "https://deno.land/std/testing/asserts.ts";

import {
  parseData,
  neighbouringCell,
  neighbouringCells,
  survivingCells,
  possibleNeighbours,
  regeneratedCells
} from './bacteria.mjs'

import DATA from './mocks/data.mjs'

import RESULTS from './mocks/results.mjs'

const parsedData = parseData( DATA )

// Rules

// 1.  Any live bacteria cell with fewer than two live neighbours dies, as if caused by under-population

// 2.  Any live bacteria cell with two or three live neighbours lives on to the next generation.

// 3.  Any live bacteria cell with more than three live neighbours dies, as if by overcrowding.

// 4.  Any dead bacteria cell with exactly three live neighbours becomes a live bacteria cell, as if by reproduction.

// false &&
test({
  name: 'Adjacent Cell To The Left',

  fn () {
    assertEquals
      ( neighbouringCell
          ( parsedData[0]
          , parsedData[1]
          )
      , true
      )

    assertEquals
      ( neighbouringCell
          ( parsedData[1]
          , parsedData[2]
          )
      , true
      )
  }
})

test({
  name: 'Adjacent Cells to the Left and Right',

  fn () {
    assertEquals
      ( neighbouringCells
          ( parsedData[1]
          , parsedData
          )
      , 2
      )

    assertEquals
      ( neighbouringCells
          ( parsedData[0]
          , parsedData
          )
      , 1
      )

    assertEquals
      ( neighbouringCells
          ( parsedData[2]
          , parsedData
          )
      , 1
      )
  }
})

test({
  name: 'Adjacent Cells in All Directions',

  fn () {
    assertEquals
      ( neighbouringCells
          ( parsedData[1]
          , [ [ 1, 2 ]
            , [ 3, 2 ]
            , [ 2, 1 ]
            , [ 2, 3 ]
            ]
          )
      , 4
      )
  }
})

test({
  name: 'Generation Survivors',

  fn () {
    assertEquals
      ( survivingCells( parsedData )
      , [ [ 2, 2 ]
        , [ 1000000002, 1000000002 ]
        ]
      )
  }
})

test({
  name: 'Cell Neighbours',

  fn () {
    assertEquals
      ( possibleNeighbours( parsedData ).length
      , 24
      )
  }
})

test({
  name: 'Regenarate Cells',

  fn () {
    assertEquals
      ( regeneratedCells( parsedData )
      , [ [ 2, 3 ]
        , [ 2, 1 ]
        , [ 1000000002, 1000000003 ]
        , [ 1000000002, 1000000001 ]
        ] 
      )
  }
})

test({
  name: 'Next Generation',

  fn () {
    const generationX =
      [ ...survivingCells( parsedData )
      , ...regeneratedCells( parsedData )
      ].sort
        ( ([ ax, ay ], [ bx, by ]) =>
            ax - bx || ay - by
        )

    assertEquals
      ( generationX 
      , RESULTS 
      )
  }
})

runTests()
