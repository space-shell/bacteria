# Bacteria growth

## Requirements

Required Software:
  - Deno

Optional Software:
  - entr

The following command installs deno for either linux or windows respectively

```shell
$ curl -fsSL https://deno.land/x/install/install.sh | sh
```

```powershell
$ iwr https://deno.land/x/install/install.ps1 -useb | iex
```

## Usage

```shell
$ cat mocks/data | deno bacteria.mjs
```

## Testing

```shell
$ deno bacteria.test.mjs
```

For development and interactive testing, entr can be used to watch for file changes and run tests ( the `-c` flag reduces screen clutter )

```shell
$ ls ./bacteria*.mjs | entr -c deno bacteria.test.mjs
```



